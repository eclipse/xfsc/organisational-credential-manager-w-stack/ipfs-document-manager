package main

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"log"
)

func createLogger(logLevel string, isDev bool) logr.Logger {
	logger, err := logr.New(logLevel, isDev, nil)
	if err != nil {
		log.Fatalf("cannot initialise logger: %t", err)
	}
	return *logger
}
