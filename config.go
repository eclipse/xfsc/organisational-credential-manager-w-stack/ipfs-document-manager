package main

import (
	"github.com/kelseyhightower/envconfig"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	"log"
)

type Config struct {
	config.BaseConfig `mapstructure:",squash"`
}

func getLoadedConfig() Config {
	var conf Config
	err := config.LoadConfig("IPFSMANAGER", &conf, nil)
	if err == nil {
		err = envconfig.Process("IPFSMANAGER", &conf)
	}
	if err != nil {
		log.Fatalf("service was not loaded: %t", err)
	}
	return conf
}
